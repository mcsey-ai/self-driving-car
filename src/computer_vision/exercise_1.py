import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import glob

img = mpimg.imread('images/cheeseboard/cheeseboard1.jpg')
plt.imshow(img)

objpoints = []
imgpoints = []

objp = np.zeros((6*8,3), np.float32)
objp[:,:2] = np.mgrid[0:8,0:6].T.reshape(-1,2)

gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)

ret, corners = cv2.findChessboardCorners(gray, (8,6), None)

if ret == True:
    imgpoints.append(corners)
    objpoints.append(objp)
    img = cv2.drawChessboardCorners(img, (8,6), corners, ret)
    plt.imshow(img)

im = cv2.imread('images/cheeseboard/cheeseboard2.png', cv2.IMREAD_GRAYSCALE)
flags = cv2.CALIB_CB_NORMALIZE_IMAGE | cv2.CALIB_CB_EXHAUSTIVE | cv2.CALIB_CB_ACCURACY
retval, corners = cv2.findChessboardCornersSB(im, (8, 5), flags=flags)
im_rgb = cv2.imread('images/cheeseboard/cheeseboard2.png', cv2.IMREAD_COLOR)
drawn = cv2.drawChessboardCorners(im_rgb, (8, 5), corners, retval)
plt.imshow(drawn)
cv2.imwrite('images/cheeseboard/corners.png', drawn)

